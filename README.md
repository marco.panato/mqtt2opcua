# MQTT 2 OPCUA

Simple python utility to map MQTT topics to an OPC-UA server.

## Requirements
Described in requirements.txt

- paho-mqtt
- opcua
- pyyaml (used for environment variables parsing)

## Usage
Set the env vars according to desired values, then run ```python3 mqtt2opcua.py```

## Docker
A dockerfile is available, equipped with ```run.sh``` and ```build.sh``` scripts.


### Configuration
To choose the topics to monitor, set the environment variable MQTT_TOPICS to this yaml value:
```
topic1/topic:           # the topic to subscribe
  type: json            # json if the payload is a json, raw if the payload is a value
  value: temperature    # if the payload is a json, the key to read
  name: Temperature     # the value of the opcua variable
topic2/a:               
  type: raw             # a topic that publishes the data directly (name of the var will be 'a' here)
```