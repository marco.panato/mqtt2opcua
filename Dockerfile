FROM python:3.8

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY mqtt2opcua.py ./

EXPOSE 48010
CMD [ "python" , "./mqtt2opcua.py" ]
