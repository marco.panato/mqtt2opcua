
import os
import ssl
import json
import yaml
import time
import opcua
import opcua.ua
import paho.mqtt.client as mqtt

# ----------------------------------------------------------------------------------------------------------------------
# Configuration
# ----------------------------------------------------------------------------------------------------------------------
mqtt_host = os.environ.get('MQTT_HOST') or '192.168.0.239'
mqtt_port = int(os.environ.get('MQTT_PORT') or '8883')
mqtt_use_tls = os.environ.get('MQTT_TLS') or False
mqtt_self_signed_cert = os.environ.get('SKIP_CERTIFICATES') or False
mqtt_user = os.environ.get('MQTT_USERNAME')
mqtt_pwd = os.environ.get('MQTT_PASSWORD')

mqtt_topics = yaml.load(os.environ.get('MQTT_TOPICS') or '')

opcua_uri = os.environ.get('OPCUA_URI') or 'opc.tcp://0.0.0.0:48010/'
# TODO authentication

log_updates = os.environ.get('LOG_UPDATES') or False


# mqtt_topics = yaml.load("""
# topic/temperatura:
#   datatype: (opcua one)
#   type: # raw  # |   #json
#   value: k (in case of json)
#   name: name  # or the last / when missing
# """)
# ----------------------------------------------------------------------------------------------------------------------


def prepare_mqtt(opcua_variables):
    print('[MAIN] Initializing MQTT client {}:{}'.format(mqtt_host, mqtt_port))
    mqttc = mqtt.Client('mqtt2opcua', clean_session=False, userdata=opcua_variables)
    if mqtt_user and mqtt_pwd:
        mqttc.username_pw_set(mqtt_user, mqtt_pwd)
    if mqtt_use_tls:
        mqttc.tls_set(ca_certs=None, certfile=None, keyfile=None, cert_reqs=ssl.CERT_NONE)  # TODO cert ?
        if mqtt_self_signed_cert:  # skip server cert validation (self signed!)
            mqttc.tls_insecure_set(True)

    def on_disconnect(client, userdata, rc):
        if rc != 0:
            print('[MQTT] Unexpected MQTT disconnection! Will reconnect...')

    def on_message(client, userdata, msg):
        data = userdata[msg.topic]
        if data.get('type') == 'json':
            value = json.loads(msg.payload).get(data['value'])
        elif data.get('type') == 'raw':
            value = msg.topic
        else:
            value = msg.topic  # TODO
        if log_updates:
            print('[MQTT] Update! {} = {}'.format(data.get('name') or msg.topic.split('/')[-1], value))
        data['var'].set_value(value)

    mqttc.on_disconnect = on_disconnect
    mqttc.on_message = on_message

    return mqttc


def prepare_opcua(variables):
    print('[MAIN] Starting OPC-UA Server {}'.format(opcua_uri))
    server = opcua.Server()
    server.set_security_policy([
        opcua.ua.SecurityPolicyType.NoSecurity,
        opcua.ua.SecurityPolicyType.Basic256Sha256_SignAndEncrypt,
        opcua.ua.SecurityPolicyType.Basic256Sha256_Sign])
    server.set_endpoint(opcua_uri)
    uri = "http://test.namespace.local"
    idx = server.register_namespace(uri)
    objects = server.get_objects_node()
    opcua_obj = objects.add_object(idx, 'MQTT')
    for t, v in variables.items():
        print('[MAIN] Registering var {}'.format(v.get('name') or t.split('/')[-1]))
        v['var'] = opcua_obj.add_variable(idx, v.get('name') or t.split('/')[-1], 0.0)

    return server


def fix_dict(variables):
    for k in variables:
        if variables[k] is None:
            variables[k] = dict()


def main():
    fix_dict(mqtt_topics)
    opcua_server = prepare_opcua(mqtt_topics)
    mqttc = prepare_mqtt(mqtt_topics)

    opcua_server.start()
    mqttc.connect(mqtt_host, mqtt_port)
    for k in mqtt_topics.keys():
        mqttc.subscribe(k, 1)
    mqttc.loop_start()

    print('[MAIN] Ready!')
    try:
        while True:
            time.sleep(3)
    except KeyboardInterrupt:
        pass
    finally:
        print('[MAIN] Closing')
        mqttc.loop_stop()
        opcua_server.stop()


if __name__ == '__main__':
    main()
