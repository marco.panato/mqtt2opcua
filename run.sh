#!/usr/bin/env bash

docker run \
  --name=Mqtt2OPCUA \
  -p 48010:48010 \
  -e "MQTT_HOST=127.0.0.1" \
  -e "MQTT_PORT=1883" \
#  -e "MQTT_TLS=True" \
#  -e "SKIP_CERTIFICATES=True" \
#  -e "MQTT_USERNAME=username" \
#  -e "MQTT_PASSWORD=password" \
  -e "OPCUA_URI=opc.tcp://0.0.0.0:48010/" \
#  -e "LOG_UPDATES=True" \
  -e "MQTT_TOPICS=
topic1/topic:
  type: json
  value: temperature
  name: Temperature
" \
  mqtt2opcua:0.1